import { useNavigation } from "@react-navigation/native"
import React, { useEffect } from "react"
import { Keyboard, Pressable, SafeAreaView, Text } from "react-native"

import styles from "./styles"
import { Colors } from "/configs"
import Routes from "/navigators/Routes"
import { responsiveH, responsiveW } from "/utils"

const Login = () => {
  const navigator = useNavigation()

  useEffect(() => {
    const keyboardDidShowListener = Keyboard.addListener("keyboardDidShow", () => {
      //
    })
    const keyboardDidHideListener = Keyboard.addListener("keyboardDidHide", () => {
      //
    })

    return () => {
      keyboardDidHideListener.remove()
      keyboardDidShowListener.remove()
    }
  }, [])

  return (
    <SafeAreaView style={styles.container}>
      <Pressable
        onPress={() => navigator.navigate(Routes.TRAFFIC)}
        style={({ pressed }) => [
          {
            backgroundColor: pressed ? Colors.white : Colors.black,
            paddingHorizontal: responsiveW(20),
            paddingVertical: responsiveH(5),
          },
        ]}
      >
        {({ pressed }) => (
          <Text style={{ fontSize: 20, color: pressed ? Colors.black : Colors.white }}>{"Biển báo giao thông"}</Text>
        )}
      </Pressable>
    </SafeAreaView>
  )
}

export default Login
