import React, { useEffect, useState } from "react"
import { Image, SafeAreaView, TouchableOpacity } from "react-native"
import { Text } from "/components"
import styles from "./styles"
import { useNavigation } from "@react-navigation/native"
import * as tf from "@tensorflow/tfjs"
import { fetch, bundleResourceIO } from "@tensorflow/tfjs-react-native"
import * as jpeg from "jpeg-js"
import { base64ImageToTensor, fontSizer, log, resizeImage, responsiveH, responsiveW, warn, width } from "/utils"
import { Colors } from "/configs"

const PATH = "../../assets/tfjs_model/"

const Traffic = () => {
  const navigator = useNavigation()
  const [imageLink, setImageLink] = useState<string>(
    "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/80_kmh_speed_limit_sign.jpg/399px-80_kmh_speed_limit_sign.jpg",
  )
  const [trafficSigns, setTrafficSigns] = useState<tf.LayersModel>()

  useEffect(() => {
    ;(async () => {
      const modelJson = require(`${PATH}model.json`)
      const modelWeightOf1 = require(`${PATH}group1-shard1of3.bin`)
      const modelWeightOf2 = require(`${PATH}group1-shard2of3.bin`)
      const modelWeightOf3 = require(`${PATH}group1-shard3of3.bin`)
      log("modelJson", modelJson)
      log("modelWeight", modelWeightOf1, modelWeightOf2, modelWeightOf3)
      try {
        const trafficModel = await tf.loadLayersModel(
          bundleResourceIO(modelJson, [modelWeightOf1, modelWeightOf2, modelWeightOf3]),
        )
        setTrafficSigns(trafficModel)
        warn("model loaded")
      } catch (error) {
        log("error", error)
      }
    })()
  }, [])

  const getImage = async () => {
    try {
      const image = await resizeImage(imageLink, 32, 32)
      const imageTensor = await base64ImageToTensor(image.base64!)
      const expandDims = await imageTensor.expandDims(0)
      const result = trafficSigns?.predict(expandDims)
      log("trafficSigns", result)
    } catch (error) {
      log("[-] Unable to load image", error)
    }
  }

  return (
    <SafeAreaView style={styles.container}>
      <Text onPress={() => navigator.goBack()} style={{ fontSize: fontSizer(20), textAlign: "center" }}>
        Nhận diện biển báo giao thông
      </Text>
      <Image source={{ uri: imageLink }} style={{ width: width, height: width }} />

      <TouchableOpacity
        style={{
          backgroundColor: Colors.black,
          paddingHorizontal: responsiveW(20),
          paddingVertical: responsiveH(5),
          justifyContent: "center",
          alignItems: "center",
        }}
        onPress={getImage}
      >
        <Text style={{ fontSize: 20, color: Colors.white }}>Predict</Text>
      </TouchableOpacity>
    </SafeAreaView>
  )
}

export default Traffic
