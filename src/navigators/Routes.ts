enum Routes {
  TEM = "Temp",
  SPLASH = "Splash",
  MAIN = "Main",
  AUTH = "Auth",
  LOGIN = "Login",
  HOME = "Home",
  TRAFFIC = "Traffic",
}

export default Routes
