import "react-native-gesture-handler"

import React from "react"
import AppNavigator from "/navigators"
import { Provider } from "react-redux"
import { PersistGate } from "redux-persist/integration/react"
import { persistor, store } from "/redux/store"
import { Platform, PermissionsAndroid } from "react-native"
import * as tf from "@tensorflow/tfjs"
import "@tensorflow/tfjs-react-native"
import { warn } from "./utils"

const Root = () => {
  React.useEffect(() => {
    ;(async () => {
      Platform.OS === "android" && (await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.CAMERA))
      Platform.OS === "android" && (await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.AUDIO_RECORDING))
    })()
  }, [])

  React.useEffect(() => {
    ;(async () => {
      try {
        await tf.ready()
        warn("tensorflow is ready")
      } catch {
        warn("tensorflow is not ready")
      }
    })()
  }, [])

  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <AppNavigator />
      </PersistGate>
    </Provider>
  )
}

export default Root
